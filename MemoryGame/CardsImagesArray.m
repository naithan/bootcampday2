//
//  CardsImagesArray.m
//  MemoryGame
//
//  Created by Jeremi Kaczmarczyk on 14.04.2015.
//  Copyright (c) 2015 Jeremi Kaczmarczyk. All rights reserved.
//

#import "CardsImagesArray.h"
#import <UIKit/UIKit.h>

@implementation CardsImagesArray

- (id)init{
    if(self == [super init]){
        UIImage *card1 = [UIImage imageNamed:@"card_front1.jpg"];
        UIImage *card2 = [UIImage imageNamed:@"card_front2.jpg"];
        UIImage *card3 = [UIImage imageNamed:@"card_front3.jpg"];
        UIImage *card4 = [UIImage imageNamed:@"card_front4.jpg"];
        UIImage *card5 = [UIImage imageNamed:@"card_front5.jpg"];
        UIImage *card6 = [UIImage imageNamed:@"card_front6.jpg"];
        UIImage *card7 = [UIImage imageNamed:@"card_front7.jpg"];
        UIImage *card8 = [UIImage imageNamed:@"card_front8.jpg"];
        
        self.imagesArray = [NSArray arrayWithObjects:card1,card2,card3,card4,card5,card6,card7,card8, nil];
    }
    return self;
}

@end
