//
//  CardCell.h
//  MemoryGame
//
//  Created by Jeremi Kaczmarczyk on 14.04.2015.
//  Copyright (c) 2015 Jeremi Kaczmarczyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@class CardsImagesArray;

@interface CardCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *cardView;
@property (nonatomic) UIImage *image;

@end
