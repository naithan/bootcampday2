//
//  CardsImagesArray.h
//  MemoryGame
//
//  Created by Jeremi Kaczmarczyk on 14.04.2015.
//  Copyright (c) 2015 Jeremi Kaczmarczyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CardsImagesArray : NSArray

@property (nonatomic, strong)NSArray *imagesArray;

@end
