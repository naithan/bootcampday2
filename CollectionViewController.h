//
//  CollectionViewController.h
//  MemoryGame
//
//  Created by Jeremi Kaczmarczyk on 14.04.2015.
//  Copyright (c) 2015 Jeremi Kaczmarczyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardsImagesArray.h"


@interface CollectionViewController : UICollectionViewController

- (NSInteger *)cardSelector:(NSInteger *)indexNumber;

@end
